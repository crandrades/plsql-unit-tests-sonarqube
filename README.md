## Descripcion

Guía con demostraciones para mejores desarrollos en [Oracle PL/SQL][oracle_plsql_url]. El foco de esta guía considera tres aspectos del desarrollo:
 - Calidad del código mediante análisis de código estático (ver [plsql-sonar](#plsql-sonar)).
 - Completitud funcional del código mediante pruebas unitarias (ver [plsql-ut](#plsql-ut)).
 - Eficiencia del código mediante revisión de la performance de ejecución (ver [plsql-performance](#plsql-performance)).

## Guías

### plsql-sonar

Esta guía se enfoca en presentar el análisis estático de código de scripts [PL/SQL][plsql_url] usando [SonarQube][sonarqube_url].

#### Inventario

1. La guía cuenta con un instructivo para replicar la demostración (ver archivo '0. Instrucciones.txt').
2. Se incluye un listado de páginas de interes como links a las respectivas URLs.

#### Dependencias

| Dependencia       | Uso                                                                                           | URL                                                                      |
| -----             | -----                                                                                         | -----                                                                    |
| Docker            | Utilizado para levantar localmente un servidor de SonarQube (1).                              | https://www.docker.com                                                   |
| Imagen SonarQube  | Imagen oficial de SonarQube en DockerHub (1).                                                 | https://hub.docker.com/_/sonarqube                                       |
| SonarQube Scanner | Cliente de SonarQube Scanner utilizado para analizar código local en un servidor SonarQube    | https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner |

- (1) Su uso es opcional. El servidor SonarQube puede ser levantado localmente mediante otros métodos, o incluso puede utilizarse un servidor SonarQube remoto.

---
### plsql-ut

Esta guía se enfoca en presentar la generación de pruebas unitarias sobre objetos [PL/SQL][plsql_url] usando [Oracle unit test][oracle_unit_test_url].

#### Inventario

1. La guía cuenta con un instructivo para replicar la demostración (ver archivo '0. Instrucciones.txt').
2. Se incluye un listado de páginas de interes como links a las respectivas URLs.
3. Se incluye script PLS para usuario DBA: Creación de usuario desarrollador y administrador de repositorio de pruebas unitarias.
4. Se incluye script PLS para usuario DEV1: Creación de objetos PL/SQL para ser probados usando pruebas unitarias.

#### Dependencias

| Dependencia                | Uso                                                              | URL                                                                    |
| -----                      | -----                                                            | -----                                                                  |
| Docker                     | Utilizado para levantar localmente un servidor de SonarQube (1). | https://www.docker.com                                                 |
| Imagen Oracle Database EE  | Imagen oficial de Oracle Database Enterprise Edition 12c (1).    | https://hub.docker.com/_/oracle-database-enterprise-edition            |
| SQL Developer              | IDE gratuito para desarrollo en Oracle PL/SQL (2).               | https://www.oracle.com/database/technologies/appdev/sql-developer.html |

- (1) Su uso es opcional. La base de datos Oracle puede ser levantada localmente mediante otros métodos, o incluso puede utilizarse una base de datos Oracle remota.
- (2) Es posible utilizar otros IDE de desarrollo que posean las mismas capacidades que el IDE SQL Developer como versión gratuita.

---
### plsql-performance

Esta guía se enfoca en presentar el análisis de performance de ejecución usando el [Execution Plan][execution_plan_url].

#### Inventario

1. Se incluye un listado de páginas de interes como links a las respectivas URLs.




[oracle_plsql_url]: https://www.oracle.com/technetwork/database/features/plsql/index.html
[plsql_url]: https://docs.oracle.com/cd/B28359_01/appdev.111/b28370/toc.htm
[sonarqube_url]: https://www.sonarqube.org/
[oracle_unit_test_url]: https://docs.oracle.com/cd/E15846_01/doc.21/e15222/unit_testing.htm
[execution_plan_url]: https://docs.oracle.com/cd/B19306_01/server.102/b14211/ex_plan.htm