CREATE TABLE employees (
    employee_id NUMBER PRIMARY KEY, 
    commission_pct NUMBER, 
    salary NUMBER
);