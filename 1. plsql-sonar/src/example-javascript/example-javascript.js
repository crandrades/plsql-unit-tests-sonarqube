// Javascript file added just to see how SonarQube reports are created

var merged = arr.reduce(function(a, b) {
    return a.concat(b);
});

// Transgressing a rule explained at https://rules.sonarsource.com/javascript/RSPEC-3828
function foo() {
  for (var i = 0; i < 5; i++) {
    yield i * 2;
  }
}