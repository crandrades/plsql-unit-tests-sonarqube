-- SEE DB VERSION
SELECT * FROM V$VERSION;

-- ALTER DBA SESSION
ALTER session SET "_ORACLE_SCRIPT" = true;

-- USER DEV1
CREATE USER DEV1 IDENTIFIED BY "pwd4dev1"  
DEFAULT TABLESPACE "USERS"
TEMPORARY TABLESPACE "TEMP";

-- SYSTEM PRIVILEGES FOR DEV1
GRANT CREATE session, CREATE table, CREATE view, CREATE any procedure, CREATE any trigger, CREATE sequence, CREATE synonym, DEBUG connect session, DEBUG any procedure to DEV1;

ALTER USER DEV1 QUOTA UNLIMITED on USERS;

-- 3.12.1 +++ NOW CREATE THE 'employees' TABLE WITH THE USER 'DEV1' and add the data (see DEV1-docker-oracle-db.pls)
-- 3.12.2 +++ NOW CREATE THE 'award_bonus' PROCEDURE WITH THE USER 'DEV1' (see DEV1-docker-oracle-db.pls)

-- 3.12.3 +++ USER UNIT_TEST_REPOS
CREATE USER UNIT_TEST_REPOS IDENTIFIED BY "pwd4unit_test_repos"  
DEFAULT TABLESPACE "USERS"
TEMPORARY TABLESPACE "TEMP";

GRANT CREATE session TO UNIT_TEST_REPOS;
ALTER USER UNIT_TEST_REPOS QUOTA UNLIMITED on USERS;

-- SQL SCRIPT AUTO-GENERATED TO GRANT 'UT_REPO_ADMINISTRATOR' PRIVILEGES TO 'UNIT_TEST_REPOS' USER
grant connect, resource, create view, unlimited tablespace to "UNIT_TEST_REPOS";

grant select on dba_roles to "UNIT_TEST_REPOS";
grant select on dba_role_privs to "UNIT_TEST_REPOS";

create role UT_REPO_ADMINISTRATOR;
create role UT_REPO_USER;
grant create public synonym,drop public synonym to UT_REPO_ADMINISTRATOR;
grant select on dba_role_privs to UT_REPO_USER;
grant select on dba_role_privs to UT_REPO_ADMINISTRATOR;
grant select on dba_roles to UT_REPO_ADMINISTRATOR;
grant select on dba_roles to UT_REPO_USER;
grant select on dba_tab_privs to UT_REPO_ADMINISTRATOR;
grant select on dba_tab_privs to UT_REPO_USER;
grant execute on dbms_lock to UT_REPO_ADMINISTRATOR;
grant execute on dbms_lock to UT_REPO_USER;
grant UT_REPO_USER to UT_REPO_ADMINISTRATOR with admin option;
grant UT_REPO_USER to DEV1;
grant UT_REPO_ADMINISTRATOR to "UNIT_TEST_REPOS" with admin option; 
commit;

-- 3.12.3 +++ NOW OPEN A CONNECTION WITH 'UNIT_TEST_REPOS' USER AND OPEN THE UNIT TEST VIEW. Then: Tools -> Unit test -> Select Current Repository

-- 3.12.4 +++ NOW PROCEED WITH SECTION 3.12.4